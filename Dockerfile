FROM ubuntu:xenial

MAINTAINER Piotr Mrowczynski <piotr.mrowczynski@cern.ch>

ARG BUILD_DATE
ARG VCS_REF

RUN apt update && \
    apt install -y \
    python-dev \
    libssl-dev \
    libxml2-dev \
    libmysqlclient-dev \
    libxslt-dev libpq-dev git \
    libffi-dev gettext \
    build-essential \
    python-pip &&  \
    pip install \
    virtualenv \
    pep8 \
    flake8 \
    tox \
    testrepository \
    git-review && \
    pip install -U virtualenv

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/tox"]

LABEL \
  org.label-schema.version="1.0" \
  org.label-schema.build-date=$BUILD_DATE \
  org.label-schema.vcs-url="https://gitlab.cern.ch/db/docker-tox.git" \
  org.label-schema.name="Tox Unit Test for Openstack" \
  org.label-schema.vendor="CERN" \
  org.label-schema.schema-version="1.0"
