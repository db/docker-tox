# Docker image simplyfing running Openstack unit tests with TOX

### Run unit tests

Go to the repository of one of Openstack components e.g. `magnum`, and run tox unit tests

```
cd /tmp/magnum
docker run -v $(pwd):/app gitlab-registry.cern.ch/db/docker-tox -e py27 tests.unit.common.x509.test_sign
```

### Run pep8 tests

```
cd /tmp/magnum
docker run -v $(pwd):/app gitlab-registry.cern.ch/db/docker-tox -e pep8 /app/magnum/tests/unit/common/x509/test_sign.py
```

### Building and publishing

Run the script which will build and publish

```
./build
```